# Wardrobify

Team:

* Rachel - Hats microservice
* Wenqi - Shoes microservice

## Overall Design Approach
In general, create RESTful APIs in microservices, integrate microservices, and write a React front-end to use our RESTful APIs.

Before starting:

1. Review microserices for shoes and hats.
2. Make sure all microservices are dockerized.
3. Check Wardrobe files, check endpoints, check CORS set up.

Steps toward final product:

1. Create models, test in admin.
2. Create RESTful API views with model encoders to get, post, delete, and create instance from models, test in Insomnia.
3. Create poller, fetching from wardrobe microservice, test in Insomnia.
4. Create front-end features with React to utilize RESTful API.

### **The existing Wardrobe API**

The existing Wardrobe API can be accessed from browser or Insomnia on port 8100.

The existing Wardrobe API can be accessed from polling service on port 8000. Your service's poller will poll the base URL `http://wardrobe-api:8000`.

It has full RESTful APIs to interact with `Bin` and `Location` resources.

## Shoes microservice

There are two Shoes microservices.

1. shoes/api: a django application, can be accessed on port 8080.

2. shoes/poll: a polling application, shoes/poll/poller.py implemented to pull Bin data from the Wardrobe API.

Models:
- Shoe model (with 4 attributes: manufacturer, model_name, color, picture_url)
- Bin model ( a value object called from wardrobe API)

Views:
- Shoe list view (respond to GET and POST method)
- Show detail view

Poller:
- retrive bin data from Wardrobe API

Front-end:
- Use React to route paths and create a single page application


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
