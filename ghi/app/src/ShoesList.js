import React from 'react';
import {NavLink} from 'react-router-dom'


function ShoesList(props) {

  const removeShoe = async(id) => {
    fetch(`http://localhost:8080/api/shoes/${id}`, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(() => {
      window.bin.reload()
    })
  }
  console.log(":::", props)
  return (
    <>
    <h1>Your shoes</h1>
    <table className="table table-hover">
      <thead>
      <tr>
        <th>Bin</th>
        <th>Shoe Color</th>
        <th>Manufacturer</th>
        <th>Model Name</th>
        <th>Picture</th>
        <th>Remove Shoe</th>

      </tr>
      </thead>
      <tbody>
				{props.shoes.map((shoe, i) => {
          return (
            <tr key={i}>
            <td> { shoe.bin } </td>
            <td> { shoe.color } </td>
            <td> { shoe.manufacturer } </td>
            <td> { shoe.model_name } </td>
            <td>
              <img src={ shoe.picture_url} className="img-thumbnail"></img>
            </td>
            <td>
              <button className="btn btn-outline-secondary" onClick={() => removeShoe(shoe.id)} type="button">
                Remove
              </button>
            </td>


            </tr>
          )
        })}
      </tbody>
    </table>
    <button>
      <NavLink className="nav-link" to="/shoes/new">Create</NavLink>
    </button>
    </>
  )
}

export default ShoesList
