import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));


async function loadShoesAndHats() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/')
  const hatsResponse = await fetch('http://localhost:8090/api/hats/')

  const shoeData = await shoesResponse.json();
  const hatData = await hatsResponse.json();


  if (shoesResponse.ok && hatsResponse.ok) {
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatData.hats} />
      </React.StrictMode>
    );


  } else {
    console.error("response not ok", shoesResponse, hatsResponse)
  }
}
loadShoesAndHats()

