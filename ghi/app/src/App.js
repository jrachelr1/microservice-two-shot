
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

import ShoesList from './ShoesList'
import ShoeForm from './ShoeForm'


function App(props) {
  if (props.hats === undefined && props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes}/>} />
          <Route path="/shoes/new" element={<ShoeForm/>} />

          <Route path="/hats" element={<HatList hats={props.hats} />} />
          <Route path="/hats/new" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
