import React from "react";

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            style_name: '',
            fabric: '',
            color: '',
            image: '',
            locations: []
        }


        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleClosetChange = this.handleClosetChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value })
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handleImageChange(event) {
        const value = event.target.value;
        this.setState({ image: value })
    }
    handleClosetChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.locations
        console.log(data)

        const hatsURL = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
        const response = await fetch(hatsURL, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()
            console.log(newHat)
        }
        const cleared = {
            style_name: '',
            fabric: '',
            color: '',
            image: '',
            locations: [],
        }
        this.setState(cleared)
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
            console.log(data)
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-location-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleStyleNameChange} value={this.state.style_name}
                                    placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleFabricChange} value={this.state.fabric}
                                    placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleColorChange} value={this.state.color}
                                    placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleImageChange} value={this.state.image}
                                    placeholder="Image" required type="url" name="image" id="image" className="form-control" />
                                <label htmlFor="image">Image</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleClosetChange}
                                    // value={this.state.locations}
                                    required name="location" id="location" className="form-select">
                                    <option value="">Choose a closet</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default HatForm