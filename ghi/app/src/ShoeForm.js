import React from 'react'


class ShoeForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      manufacturer: "",
      color: "",
      picture: "",
      bin: "",
      bins: [],
    }
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
    this.handleColorChange = this.handleColorChange.bind(this)
    this.handleBinChange = this.handleBinChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleNameChange(event) {
    const value = event.target.value
    this.setState({name: value})
  }
  handleManufacturerChange(event) {
    const value = event.target.value
    this.setState({manufacturer: value})
  }
  handleColorChange(event) {
    const value = event.target.value
    this.setState({color: value})
  }
  handleBinChange(event) {
    const value = event.target.value
    this.setState({bin: value})
  }
  handlePictureChange(event) {
    const value = event.target.value
    this.setState({picture: value})
  }
  async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state}
    data.picture_url = data.picture
    data.model_name = data.name
    delete data.name
    delete data.picture
    delete data.bins
    console.log(data)

    const shoeUrl = 'http://localhost:8080/api/shoes/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      const newShoe = await response.json()
      console.log("new shoe:::", newShoe)

      const cleared = {
        name: "",
        manufacturer: "",
        color: "",
        picture:"",
        bin: "",
      }
      this.setState(cleared)
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()

      this.setState({bins: data.bins})
    }
  }
  render () {
    return(
      <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Add new shoes</h1>
                  <form onSubmit={this.handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                      <label htmlFor="name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                      <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="Color" id="Color" className="form-control"/>
                      <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handlePictureChange} value={this.state.picture} placeholder="picture" required type="url" name="picture" id="picture" className="form-control"/>
                      <label htmlFor="picture">Picture URL</label>
                    </div>
                    <div className="mb-3">
                      <select onChange={this.handleBinChange} required id="bin" name="bin" className="form-select">
                        <option value="">Choose a bin</option>
                          {this.state.bins.map(bin => {
                            return (
                              <option value={bin.id} key={bin.id}>
                                {bin.closet_name}
                              </option>
                            )
                          })}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
    )
  }
}

export default ShoeForm
