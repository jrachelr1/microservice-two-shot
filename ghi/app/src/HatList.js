import React, { useState } from 'react';


function HatList(props) {

    const deleteHat = async (id) => {
        fetch(`http://localhost:8090/api/hats/${id}`, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            window.location.reload();
        })
    }

    console.log(props)
    return (
        <>
            <h1>Your hats</h1>
            <div>You have {props.hats?.length} hats</div>

            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Style Name</th>
                            <th>Fabric</th>
                            <th>Color</th>
                            {/* <th>Image</th> */}
                            <th>Closet Name</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.hats && props.hats.map(hat => {
                            return (
                                <tr key={hat.id}>
                                    <td>{hat.style_name}</td>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.color}</td>
                                    {/* <td>{hat.image}</td> */}
                                    <td>{hat.location.closet_name}</td>
                                    <td>
                                        <button className="btn btn-outline-dark" onClick={() => deleteHat(hat.id)} type="button" >Remove</button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </>
    )


}
export default HatList;
