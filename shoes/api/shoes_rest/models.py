from django.db import models
from django.urls import reverse

# Create your models here.



class BinVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    # def get_api_url(self):
    #     return reverse("api_location", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")


class Shoe(models.Model):
  manufacturer = models.CharField(max_length=120)
  model_name = models.CharField(max_length=200)
  color = models.CharField(max_length=200)
  picture_url = models.URLField(blank=True, null=True)

  bin = models.ForeignKey(
    BinVO,
    related_name="shoes",
    on_delete=models.PROTECT,
    null=True,
  )

  def __str__(self):
    return self.model_name
