from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeListEncoder(ModelEncoder):
  model = Shoe
  properties = ["id", "manufacturer", "model_name", "color", "picture_url"]
  def get_extra_data(self, o):
    return{"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
  model = Shoe
  properties = ["id", "manufacturer", "model_name", "color", "picture_url"]

  encoders = {
    "bin": BinVOEncoder(),
  }



@require_http_methods(["GET", "POST"])
def api_list_shoe(request, bin_vo_id=None):
  if request.method == "GET":
    if bin_vo_id is not None:
      shoes = Shoe.objects.filter(bin=bin_vo_id)
    else:
      shoes = Shoe.objects.all()
      return JsonResponse(
        {"shoes": shoes},
        encoder=ShoeListEncoder,
      )
  else: #POST
    content = json.loads(request.body)
    try:
      bin_href = content["bin"]
      print("bin_data:::", bin_href)
      bin = BinVO.objects.get(bin_number=bin_href)
      content["bin"] = bin
    except BinVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid bin ID"},
        status = 400,
      )

    shoe = Shoe.objects.create(**content)
    return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
  if request.method == "GET":
    try:
      shoe = Shoe.objects.get(id=pk)
      print(":::", id)
      return JsonResponse(
          shoe,
          encoder=ShoeDetailEncoder,
          safe=False,
      )
    except Shoe.DoesNotExist:
      response = JsonResponse({"message": "Does not exist"})
      response.status_code = 404
      return response
  elif request.method == "DELETE":
      count, _ = Shoe.objects.filter(id=pk).delete()
      return JsonResponse({"deleted": count > 0})
