from .views import api_location, api_locations, api_list_hats, api_hat_detail
from django.urls import path

urlpatterns = [
    path("locations/", api_locations, name="api_locations"),
    path("locations/<int:pk>/", api_location, name="api_location"),
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>", api_hat_detail, name="api_list_hats"),
]
