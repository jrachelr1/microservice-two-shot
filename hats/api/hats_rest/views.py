from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["id", "closet_name", "section_number", "shelf_number", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "location_id",
        "id",
        "fabric",
        "color",
        "image",
        "location",
    ]

    encoders = {"location": LocationVOEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "id",
        "fabric",
        "color",
        "image",
        "location_id",
        "location",
    ]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_locations(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationVOEncoder,
        )
    else:
        content = json.loads(request.body)
        location = LocationVO.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationVOEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_location(request, pk):
    if request.method == "GET":
        try:
            location = LocationVO.objects.get(id=pk)
            return JsonResponse(location, encoder=LocationVOEncoder, safe=False)
        except LocationVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            location = LocationVO.objects.get(id=pk)
            location.delete()
            return JsonResponse(
                location,
                encoder=LocationVOEncoder,
                safe=False,
            )
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            location = LocationVO.objects.get(id=pk)

            props = ["closet_name", "shelf_number", "section_number"]
            for prop in props:
                if prop in content:
                    setattr(location, prop, content[prop])
            location.save()
            return JsonResponse(
                location,
                encoder=LocationVOEncoder,
                safe=False,
            )
        except LocationVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat_detail(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            hat.save()
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
